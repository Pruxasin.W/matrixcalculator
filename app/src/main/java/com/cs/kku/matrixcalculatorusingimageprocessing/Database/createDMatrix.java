package com.cs.kku.matrixcalculatorusingimageprocessing.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Anata on 17/10/2558.
 */
public class createDMatrix extends SQLiteOpenHelper {
    private static final String DB_NAME = "Matrix";
    private static final int DB_VERSION = 1;

    public static final String TABLE_NAME_DATA = "DataMatrix";
    public static final String TABLE_NAME_SAVE = "SaveDataMatrix";

    public static final String DATA_ID = "data_num";
    public static final String DATA_MRow = "data_row";
    public static final String DATA_MColumn = "data_column";
    public static final String DATA_MData = "data_data";

    public static final String SAVE_ID = "save_num";
    public static final String SAVE_Name = "save_name";

    private static final String CREATE_DATA_MATRIX = "CREATE TABLE " + TABLE_NAME_DATA + " (" + DATA_ID + " INTEGER PRIMARY KEY, "
             + DATA_MRow + " INTEGER," + DATA_MColumn + " INTEGER," + DATA_MData + " TEXT);";

    public createDMatrix(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_DATA);
        onCreate(db);
    }
}