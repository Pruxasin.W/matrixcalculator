package com.cs.kku.matrixcalculatorusingimageprocessing.Database;

import android.util.Log;

/**
 * Created by Anata on 18/10/2558.
 */
public class ClassifyTypeMatrix {

    String Ans;
    int countLine = 1;

    public String Classify(String Ans) {

        String Answer = Ans;
        String[] matrixClassify;
        String[] matrixPosition;
        String[] matrixData;


        Answer = Answer.replaceAll("[\\t\\n\\r]", " ");
        Answer = Answer.replaceAll("[iIl]", "1");
        Answer = Answer.replaceAll("[oO]", "0");
        Answer = Answer.replaceAll("[sS]", "5");
        Answer = Answer.replaceAll("[Zz]", "2");
        Answer = Answer.replaceAll("4-", "4");
        Answer = Answer.replaceAll("[*]", "");
        Answer = Answer.replaceAll("[']", "");
        Answer = Answer.replaceAll("[qQB]", "8");
        Answer = Answer.replaceAll("[_#]", "-");

        matrixClassify = Answer.split(" p ");


        matrixPosition = matrixClassify[1].split(" ");
        matrixData = matrixClassify[0].split(" ");


        String newLine = "";
        String newMatrix = "";

        int[] intPosition = new int[matrixPosition.length - 1];
        String anserint = "";


        for (int i = 0; i < matrixPosition.length - 1; i++) {
            if (i < matrixPosition.length - 1) {
                intPosition[i] = Math.abs(Integer.parseInt(matrixPosition[i]) - Integer.parseInt(matrixPosition[i + 1]));
                //if ((Math.abs(Integer.parseInt(matrixPosition[i]) - Integer.parseInt(matrixPosition[i + 1])) >= Integer.parseInt(matrixPosition[i])*2)) {

                if (intPosition[i] >= (intPosition[0] + (intPosition[0] / 4))) {
                    newMatrix += i + " " + (i + 1) + " ";

                    if (intPosition[i] >= 4 * intPosition[0]) {
                        newLine += i + " " + (i + 1) + " ";
                        countLine++;
                    }
                }
                anserint += intPosition[i] + " ";
            }
        }

        String PositionMatrixOne = "";
        String PositionMatrixTwo = "";
        String PositionOperatorMatrix = "";
        String matrixOne = "";
        String matrixTwo = "";
        String matrixOperator = "";
        String matrixSize = "";
        String rowMatrixOne = "";
        String columnMatrixOne = "";
        String rowMatrixTwo = "";
        String columnMatrixTwo = "";
        String[] sizeOne_a;
        String[] sizeTwo_b;

        if (countLine > 1) {

            String[] branchMatrix;
            branchMatrix = newMatrix.split(" ");

            for (int j = 0; j < Integer.parseInt(branchMatrix[0]) + 1; j++) {
                for (int i = 0; i < matrixPosition.length; i++) {
                    if ((Integer.parseInt(matrixPosition[i]) <= Integer.parseInt(matrixPosition[j]) + 10) && (Integer.parseInt(matrixPosition[i]) >= Integer.parseInt(matrixPosition[j]) - 10)) {

                        PositionMatrixOne += i + " ";
                        matrixOne += matrixData[i] + " ";
                        rowMatrixOne += i + " ";
                    }
                }
                rowMatrixOne += "a";
            }

            for (int j = Integer.parseInt(branchMatrix[1]); j < Integer.parseInt(branchMatrix[2]) + 1; j++) {
                for (int i = 0; i < matrixPosition.length; i++) {
                    if ((Integer.parseInt(matrixPosition[i]) <= Integer.parseInt(matrixPosition[j]) + 10) && (Integer.parseInt(matrixPosition[i]) >= Integer.parseInt(matrixPosition[j]) - 10)) {

                        PositionMatrixTwo += i + " ";
                        matrixTwo += matrixData[i] + " ";
                        rowMatrixTwo += i + " ";
                    }
                }
                rowMatrixTwo += "b";
            }

            for (int i = 0; i < matrixPosition.length; i++) {
                String positionAll;
                String searchOperator;

                searchOperator = "" + i;
                positionAll = PositionMatrixOne + PositionMatrixTwo;
                Boolean noFound;
                noFound = positionAll.contains(searchOperator);
                if (noFound == false) {
                    PositionOperatorMatrix = "" + i;
                    matrixOperator += " " + matrixData[i];
                }
            }

            sizeOne_a = rowMatrixOne.split("a");
            String[] sizeRowOne = sizeOne_a[0].split(" ");
            sizeTwo_b = rowMatrixTwo.split("b");
            String[] sizeRowTwo = sizeTwo_b[0].split(" ");
            matrixSize = sizeRowOne.length + " " + branchMatrix[1] + " " + sizeRowTwo.length + " " + (Integer.parseInt(branchMatrix[3]) - Integer.parseInt(branchMatrix[1]));

            /*
            for (int j = 0; j < Integer.parseInt(firstLine[0]) + 1; j++) {
                for (int i = 0; i < matrixPosition.length; i++) {
                    if ((Integer.parseInt(matrixPosition[i]) <= Integer.parseInt(matrixPosition[j]) + 10) && (Integer.parseInt(matrixPosition[i]) >= Integer.parseInt(matrixPosition[j]) - 10)) {

                        columnPosition += i + " ";
                    }
                    /*
                    else if ((Integer.parseInt(matrixPosition[i]) <= Integer.parseInt(matrixPosition[j]) + 30) && (Integer.parseInt(matrixPosition[i]) >= Integer.parseInt(matrixPosition[j]) - 30)) {
                        operatorMatrix += i;
                    }

                }
                columnPosition += "a ";
            }
            */
            Ans = "m " + matrixOne + "m " + matrixTwo + "m" + matrixOperator + " m " + matrixSize;


        } else {
            int count =1;

            for (int i = 0; i < matrixPosition.length; i++) {
                if (Integer.parseInt(matrixPosition[i]) < Integer.parseInt(matrixPosition[i+1]) ) {
                    count++;

                }else {
                    break;
                }
            }

            for (int j = 0; j < count; j++) {
                for (int i = 0; i < matrixPosition.length; i++) {
                    if ((Integer.parseInt(matrixPosition[i]) <= Integer.parseInt(matrixPosition[j]) + 10) && (Integer.parseInt(matrixPosition[i]) >= Integer.parseInt(matrixPosition[j]) - 10)) {

                        PositionMatrixOne += i + " ";
                        matrixOne += matrixData[i] + " ";
                        rowMatrixOne += i + " ";
                    }
                }
                rowMatrixOne += "a";
            }
            sizeOne_a = rowMatrixOne.split("a");
            String[] sizeRowOne = sizeOne_a[0].split(" ");
            matrixSize = sizeRowOne.length + " " + count;
            Ans = "m "+ matrixOne + "m "+ matrixSize;

            return Ans;
        }

        //return " บรรทัดใหม่= " + newLine + " จำนวนบรรทัด= " + countLine + " เมทริกซทใหม่= " + newMatrix + "Column = " +columnPosition;
        //return  ""+matrixPosition.length;
        //return columnPosition + " " + operatorMatrix;
        //return "เมทริกซ์ที่ One "+PositionMatrixOne + "เมทริกซ์ที่ Two "+ PositionMatrixTwo + "เครื่องหมาย " + PositionOperatorMatrix + "matrixOne " + matrixOne + "matrixTwo " + matrixTwo;
        return Ans;
    }

    public boolean typeMatrixOne() {
        if (countLine > 1) {
            return true;
        }
        return false;
    }

}
